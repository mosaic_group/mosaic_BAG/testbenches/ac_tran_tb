"""
Testbench ac_tran_tb
====================

"""

from sal.testbench_base import TestbenchBase
from .params import ac_tran_tb_params


class testbench(TestbenchBase):
    def __init__(self):
        super().__init__()
        self.params = None

    @property
    def package(self):
        return "ac_tran_tb"

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return ac_tran_tb_params

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> ac_tran_tb_params:
        return self._params

    @params.setter
    def params(self, val: ac_tran_tb_params):
        self._params = val
