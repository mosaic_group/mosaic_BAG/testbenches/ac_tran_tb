v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
C {devices/ipin.sym} 80 -55 0 0 {name=p1 lab=vin}
C {devices/opin.sym} 100 -55 0 0 {name=p2 lab=vout}
C {devices/iopin.sym} 80 -120 0 1 {name=p3 lab=VDD}
C {devices/iopin.sym} 80 -100 0 1 {name=p4 lab=VSS}
C {devices/ipin.sym} 80 -35 0 0 {name=p5 lab=ibias}
